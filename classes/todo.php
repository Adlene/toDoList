<?php
    include("db.php");

    class ToDo{
        private $text = "";
        private $done = false;
        private $id;

        function __construct($text, $done){
            $this->setText($text);
            $this->setDone($done);
            $this->id=uniqid();
        }

        public function getText(){
            return $this->text;
        }
        public function setText($a){
            $this->text=$a;
        }
        public function setDone($a){
            $this->done=$a;
        }
        public function save(){
            $db=new Db;
            $db->addToDo($this);
        }

    }

?>