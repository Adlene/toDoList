<?php
/*classe de la base de donnée*/
class Db{

    public static function connection() {
        $user='root';
        $password='aniel01280';
        $host='localhost';
        $database='projettodolist';
        $bdd = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password);
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        return $bdd;
    }
    public function addToDo($data){
        $finaldata = $data->getText();
        $bdd = self::connection();
        $response = $bdd->prepare("INSERT INTO todo(texte) VALUES(:text)");
        $response->execute([
            'text'=>$finaldata,
        ]);
    }
    public function updateToDo($data, $id){
        $bdd = self::connection();
        $response = $bdd->prepare('UPDATE todo SET texte = :text WHERE id=:id');
        $response->execute([
            'text'=>$data,
            'id'=>$id,
        ]);
    }
    public function read () {
        $bdd = self::connection();
        $response = $bdd->query('SELECT * FROM todo');
        return $response->fetchAll(PDO::FETCH_ASSOC);
    }
    public function write($data) {
        // J'enregistre les données dans le fichier, en les sérialisant
        file_put_contents(__DIR__."/../db/todos.txt", serialize($data));
    }
    public function deleteToDo($id){
        $bdd = self::connection();
        $response=$bdd->prepare('DELETE FROM todo WHERE id=:id');
        $response->execute([
            'id'=>$id,
        ]);
    }
}

?>