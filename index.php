<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css"  href="asset/toDoList.css"></link>
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <?php
        include("classes/todo.php");
        include_once(__DIR__."/classes/db.php");
 /*test de lecture des données de la DB*/
        $dbb= new Db;
        $response=$dbb->read();

    ?>
    <div class= "list">
    <h1>TO-DO LIST</h1>

    <div class="affichageToDo">
    <?php
        // J'appel ma DB pour lire le fichier
        $db = new DB();
        $listTodo = $db->read();
    ?>
    <?php
        // J'ai ma liste de todo, j'affiche le text de chaque todo
        foreach ($listTodo as $todo) { ?>
            <form method="POST" action="forms/updateToDo.php" class="updateToDo name="updateToDo">
            <input type="textarea" name="text" class="uneToDo" value="<?php echo $todo['texte']; ?>">
            <input type="hidden" name="id" value="<?php echo $todo['id']; ?>">
            <button type="submit" name= "updateButton">
            <img src=asset/update.png>
            </button>
            </form>
            <form method="POST" action="forms/deleteToDo.php">
                <input type="hidden" name="id" value="<?php echo $todo['id']; ?>">
                <button type="submit" class="deleteButton name="deleteButton">
                <img src=asset/del.png>
                </button>
            </form>
        <?php } ?>
    </div>
        <span>Enregistrer une tâche:</span>
    <form method="POST" action="forms/addToDo.php" class="addToDo"
    name="addToDo">
    <input type="textarea" class="inputAdd" name="text">
    <button type="submit"><img src="asset/save.png"></button>
    </form>
    </div>
</body>
</html>